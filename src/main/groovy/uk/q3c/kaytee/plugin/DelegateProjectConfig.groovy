package uk.q3c.kaytee.plugin

import org.apache.commons.lang.StringUtils

/**
 * Created by David Sowerby on 05 Jun 2017
 */
class DelegateProjectConfig {
    String url = ""
    String taskToRun = "test"
    String branch = "develop"
    String commitId = ""

    DelegateProjectConfig() {
    }

    DelegateProjectConfig(DelegateProjectConfig other) {
        url = other.url
        taskToRun = other.taskToRun
        branch = other.branch
        commitId = other.commitId
    }

    def validate(TaskKey group, List<String> errors) {
        String groupLog = "Test group '${group.name()}'"
        if (StringUtils.isEmpty(commitId)) {
            errors.add("$groupLog: commitId cannot be null or empty for a delegated task")
        }
        if (StringUtils.isEmpty(url)) {
            errors.add("$groupLog: url cannot be null or empty for a delegated task")
        }
        if (StringUtils.isEmpty(taskToRun)) {
            errors.add("$groupLog: taskToRun cannot be null or empty for a delegated task")
        }
        if (StringUtils.isEmpty(branch)) {
            errors.add("$groupLog: branch cannot be null or empty for a delegated task")
        }
    }

    boolean equals(o) {
        if (this.is(o)) return true
        if (getClass() != o.class) return false

        DelegateProjectConfig that = (DelegateProjectConfig) o

        if (url != that.url) return false
        if (branch != that.branch) return false
        if (commitId != that.commitId) return false
        if (taskToRun != that.taskToRun) return false

        return true
    }

    int hashCode() {
        int result
        result = url.hashCode()
        result = 31 * result + taskToRun.hashCode()
        result = 31 * result + branch.hashCode()
        result = 31 * result + commitId.hashCode()
        return result
    }
}
